library ieee;
use ieee.std_logic_1164.all;

entity mux41e is port (
	d: in std_logic_vector(0 to 3);
	a: in std_logic_vector(1 downto 0);
	e: in std_logic;
	y: out std_logic
);
end mux413;

architecture ponasajna of mux41e is
begin

	process(a,e,d) --lista osjetljivosti
	begin
		if e = '0' then
			t <= '0';
		else
			case a is
				when "00" => y <= d(0);
				when "01" => y <= d(1);
				when "10" => y <= d(2);
				when "11" => y <= d(3);
				when others => y <= '0';
			end case;
		end if;
		
	end process;
end ponasanja;
