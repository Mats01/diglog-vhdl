library ieee;
use ieee.std_logic_1164.all;

entity veliki is port  (
	a1,a0,e: in std_logic;
	o0,o1,o2,o3: out std_logic

);
end veliki;

architecture str of veliki is
	signal ec, ecc: std_logic;
begin

	c1: entity work.mali port map (
	--povezivanje putem imena
		e => e,
		a => a1,
		o0 => ec,
		o1 => ecc 
	);
	c2: entity work.mali port map (a0, ec, o0, o1); --povezivanje putem pozicije
	c3: entity work.mali port map (a0, ecc, o2, o3); --povezivanje putem pozicije
	
end str;
	
		
		
