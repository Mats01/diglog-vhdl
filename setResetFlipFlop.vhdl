library ieee;
use ieee.std_logic_1164.all;

entity srff is port (
  s, r, cp, sdn, cdn: in std_logic;
  q, qn: out std_logic
  
);
end srff;

architecture arch of srff is 
  signal sni, rni, qi, qni: std_logic;
begin
  sni <= s nand cp;
  rni <= r nand cp;
  qi  <= not (sni and sdn and qni);
  qni <= not (rni and cdn and qi);
  q  <= qi;
  qn <= qni;
end arch;
