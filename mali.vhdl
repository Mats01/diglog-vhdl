library ieee;
use ieee.std_logic_1164.all;

entity mali is port (
	a,e: in std_logic;
	o1,o0: out std_logic

);
end mali;

architecture arch of mali is
begin
	o0 <= e and not a after 500ms;
	o1 <= e and a after 500ms;
end arch;
