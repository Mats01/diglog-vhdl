library ieee;
use ieee.std_logic_1164.all;

entity first is port (
	a, b,c: in std_logic;
	x: out std_logic);
end first;

architecture arch of first is
begin
	x <= a and b and c;
end arch;
